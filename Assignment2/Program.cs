﻿namespace Assignment2 {
    internal class Program {
        static void Main(string[] args) {
            Console.WriteLine(Ex1(1));
            Console.WriteLine(Ex2(new int[] { 2, -6, 2, -1 }, 3));
            Console.WriteLine(Ex2(new int[] { 2, 0, 3, 1 }, 2));
        }

        private static int Ex1(int x) => Convert.ToInt32(2 * Math.Pow(x, 3) - 6 * Math.Pow(x, 2) + 2 * x - 1);

        private static int Ex2(int[] poly, int x) {
            poly = poly.Reverse().ToArray();
            int ret = 0;
            for (int i = 0; i < poly.Length; i++) {
                ret += poly[i] * Convert.ToInt32(Math.Pow(x, i));
            }
            return ret;
        }
    }
}